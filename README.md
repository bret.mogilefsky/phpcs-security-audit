# phpcs-security-audit analyzer

This analyzer is a wrapper around [phpcs-security-audit](https://github.com/FloeDesignTechnologies/phpcs-security-audit),
a set of [PHP CodeSniffer](https://github.com/squizlabs/PHP_CodeSniffer) rules
that finds vulnerabilities and weaknesses related to security in PHP code.
It's is written in Go using
the [common library](https://gitlab.com/gitlab-org/security-products/analyzers/common)
shared by all analyzers.

The [common library](https://gitlab.com/gitlab-org/security-products/analyzers/common)
contains documentation on how to run, test and modify this analyzer.

## Contributing

Contributions are welcome, see [`CONTRIBUTING.md`](CONTRIBUTING.md) for more details.

## License

This code is distributed under the MIT license, see the [LICENSE](LICENSE) file.
