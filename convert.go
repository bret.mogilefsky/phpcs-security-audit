package main

import (
	"encoding/json"
	"io"
	"os"
	"path/filepath"
	"strings"

	"gitlab.com/gitlab-org/security-products/analyzers/common/issue"
)

const toolID = "phpcs_security_audit"

func convert(reader io.Reader, prependPath string) ([]issue.Issue, error) {
	var report Report

	err := json.NewDecoder(reader).Decode(&report)
	if err != nil {
		return nil, err
	}

	// HACK: extract root path from environment variables
	root := os.Getenv("ANALYZER_TARGET_DIR")
	if root == "" {
		root = os.Getenv("CI_PROJECT_DIR")
	}

	// Process PHP CodeSniffer report
	issues := []issue.Issue{}
	for path, fileReport := range report.Files {
		for _, m := range fileReport.Messages {
			rel := filepath.Join(prependPath, strings.TrimPrefix(path, root))
			issues = append(issues, issue.Issue{
				Category:    issue.CategorySast,
				Tool:        toolID,
				Name:        m.Message,
				Message:     m.Message,
				CompareKey:  m.CompareKey(rel),
				Severity:    m.Severity(),
				Location:    m.Location(rel),
				Identifiers: m.Identifiers(),
			})
		}
	}
	return issues, nil
}

// Report is a report returned by PHP CodeSniffer.
type Report struct {
	Files map[string]FileReport
}

// FileReport is the section of a report for one specific source file.
type FileReport struct {
	Messages []Message
}

// Message is a vulnerability found in the source code.
// It's either an error or a warning message.
type Message struct {
	// Fixable bool // Fixable is always false
	Column  int
	Source  string // Source refers to a class like PHPCS_SecurityAudit.BadFunctions.EasyXSS.EasyXSSwarn
	Message string
	// Severity int // Severity is always 5
	Type string // Type is either WARNING or ERROR
	Line int
}

// CompareKey returns a string used to establish whether two issues are the same.
// It takes the relative path of the affected file since it's not known in this context.
func (m Message) CompareKey(filepath string) string {
	return strings.Join([]string{filepath, m.Source}, ":") // NOTE: no line number
}

// Severity returns the normalized severity.
func (m Message) Severity() issue.Level {
	switch m.Type {
	case "ERROR":
		return issue.LevelHigh
	case "WARNING":
		return issue.LevelLow
	}
	return issue.LevelUnknown
}

// Location returns a structured Location
func (m Message) Location(rel string) issue.Location {
	return issue.Location{
		File:      rel,
		LineStart: m.Line,
	}
}

// Identifiers returns the normalized identifiers of the vulnerability.
func (m Message) Identifiers() []issue.Identifier {
	return []issue.Identifier{
		m.PSAIdentifier(),
	}
}

// PSAIdentifier returns a structured Identifier for a PHPCS_SecurityAudit source type
// ex: PHPCS_SecurityAudit.BadFunctions.PregReplace.PregReplaceE
func (m Message) PSAIdentifier() issue.Identifier {
	return issue.Identifier{
		Type:  "phpcs_security_audit_source",
		Name:  m.Source,
		Value: m.Source,
	}
}
