package main

import (
	"os"
	"reflect"
	"strings"
	"testing"

	"gitlab.com/gitlab-org/security-products/analyzers/common/issue"
)

func init() {
	os.Setenv("CI_PROJECT_DIR", "/home/php")
}

func TestConvert(t *testing.T) {
	in := `{
   "totals" : {
      "errors" : 2,
      "warnings" : 2,
      "fixable" : 0
   },
   "files" : {
      "/home/php/tests.php" : {
         "errors" : 1,
         "warnings" : 1,
         "messages" : [
            {
               "fixable" : false,
               "column" : 14,
               "source" : "PHPCS_SecurityAudit.BadFunctions.EasyXSS.EasyXSSwarn",
               "message" : "Possible XSS detected with . on echo",
               "severity" : 5,
               "type" : "WARNING",
               "line" : 6
            },
            {
               "fixable" : false,
               "column" : 16,
               "source" : "PHPCS_SecurityAudit.BadFunctions.EasyXSS.EasyXSSerr",
               "message" : "Easy XSS detected because of direct user input with $_POST on echo",
               "severity" : 5,
               "type" : "ERROR",
               "line" : 6
            }
         ]
      },
      "/home/php/xyz/more-tests.php" : {
         "errors" : 1,
         "warnings" : 1,
         "messages" : [
            {
               "fixable" : false,
               "column" : 2,
               "source" : "PHPCS_SecurityAudit.BadFunctions.PregReplace.PregReplaceE",
               "message" : "Usage of preg_replace with /e modifier is not recommended.",
               "severity" : 5,
               "type" : "WARNING",
               "line" : 10
            },
            {
               "message" : "User input and /e modifier found in preg_replace, remote code execution possible.",
               "source" : "PHPCS_SecurityAudit.BadFunctions.PregReplace.PregReplaceUserInputE",
               "column" : 2,
               "fixable" : false,
               "line" : 10,
               "type" : "ERROR",
               "severity" : 5
            }
         ]
      }
   }
} `
	r := strings.NewReader(in)
	want := []issue.Issue{
		{
			Category:   issue.CategorySast,
			Tool:       "phpcs_security_audit",
			Name:       "Possible XSS detected with . on echo",
			Message:    "Possible XSS detected with . on echo",
			CompareKey: "app/tests.php:PHPCS_SecurityAudit.BadFunctions.EasyXSS.EasyXSSwarn",
			Severity:   issue.LevelLow,
			Location: issue.Location{
				File:      "app/tests.php",
				LineStart: 6,
			},
			Identifiers: []issue.Identifier{
				{
					Type:  "phpcs_security_audit_source",
					Name:  "PHPCS_SecurityAudit.BadFunctions.EasyXSS.EasyXSSwarn",
					Value: "PHPCS_SecurityAudit.BadFunctions.EasyXSS.EasyXSSwarn",
				},
			},
		},
		{
			Category:   issue.CategorySast,
			Tool:       "phpcs_security_audit",
			Name:       "Easy XSS detected because of direct user input with $_POST on echo",
			Message:    "Easy XSS detected because of direct user input with $_POST on echo",
			CompareKey: "app/tests.php:PHPCS_SecurityAudit.BadFunctions.EasyXSS.EasyXSSerr",
			Severity:   issue.LevelHigh,
			Location: issue.Location{
				File:      "app/tests.php",
				LineStart: 6,
			},
			Identifiers: []issue.Identifier{
				{
					Type:  "phpcs_security_audit_source",
					Name:  "PHPCS_SecurityAudit.BadFunctions.EasyXSS.EasyXSSerr",
					Value: "PHPCS_SecurityAudit.BadFunctions.EasyXSS.EasyXSSerr",
				},
			},
		},
		{
			Category:   issue.CategorySast,
			Tool:       "phpcs_security_audit",
			Name:       "Usage of preg_replace with /e modifier is not recommended.",
			Message:    "Usage of preg_replace with /e modifier is not recommended.",
			CompareKey: "app/xyz/more-tests.php:PHPCS_SecurityAudit.BadFunctions.PregReplace.PregReplaceE",
			Severity:   issue.LevelLow,
			Location: issue.Location{
				File:      "app/xyz/more-tests.php",
				LineStart: 10,
			},
			Identifiers: []issue.Identifier{
				{
					Type:  "phpcs_security_audit_source",
					Name:  "PHPCS_SecurityAudit.BadFunctions.PregReplace.PregReplaceE",
					Value: "PHPCS_SecurityAudit.BadFunctions.PregReplace.PregReplaceE",
				},
			},
		},
		{
			Category:   issue.CategorySast,
			Tool:       "phpcs_security_audit",
			Name:       "User input and /e modifier found in preg_replace, remote code execution possible.",
			Message:    "User input and /e modifier found in preg_replace, remote code execution possible.",
			CompareKey: "app/xyz/more-tests.php:PHPCS_SecurityAudit.BadFunctions.PregReplace.PregReplaceUserInputE",
			Severity:   issue.LevelHigh,
			Location: issue.Location{
				File:      "app/xyz/more-tests.php",
				LineStart: 10,
			},
			Identifiers: []issue.Identifier{
				{
					Type:  "phpcs_security_audit_source",
					Name:  "PHPCS_SecurityAudit.BadFunctions.PregReplace.PregReplaceUserInputE",
					Value: "PHPCS_SecurityAudit.BadFunctions.PregReplace.PregReplaceUserInputE",
				},
			},
		},
	}
	got, err := convert(r, "app")
	if err != nil {
		t.Fatal(err)
	}
	if !reflect.DeepEqual(want, got) {
		t.Errorf("Wrong result. Expected:\n%#v\nbut got:\n%#v", want, got)
	}
}
